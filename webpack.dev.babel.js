/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */

import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import path from 'path';
import fs from 'fs';

const files = fs.readdirSync(path.join(__dirname, 'src/scripts'));

const htmlPlugins = files.map((file) => {
  const fileName = path.basename(file, '.js');

  let template;
  if (fileName === 'index') {
    template = path.join(__dirname, 'src/templates/index.hbs');
  } else {
    template = path.join(__dirname, 'src/templates/country.hbs');
  }

  return new HtmlWebpackPlugin({
    template,
    inject: true,
    chunks: [fileName, 'vendor'],
    filename: `${fileName}.html`,
    favicon: 'src/images/favicon.ico',
  });
});

const entry = Object.assign({
  vendor: ['geodashboard'],
}, files.reduce((obj, file) => {
  obj[path.basename(file, '.js')] = path.join(__dirname, 'src/scripts', file);
  return obj;
}, {}));

module.exports = {
  entry,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  devtool: 'source-map',
  module: {
    noParse: /geodashboard/,
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, 'src'),
        ],
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader',
        include: [
          path.resolve(__dirname, 'src'),
        ],
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap'],
        include: [
          path.resolve(__dirname, 'src'),
        ],
      },
      {
        test: /\.(jpg|jpeg|gif|png|ico)$/,
        loader: 'file-loader',
        include: [
          path.resolve(__dirname, 'src'),
        ],
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: path.join(__dirname, 'src/locales'),
      to: 'locales',
    }, {
      from: path.join(__dirname, 'src/images'),
      to: 'images',
    }]),
  ].concat(htmlPlugins),
  devServer: {
    port: 9000,
    stats: {
      colors: true,
    },
  },
};
