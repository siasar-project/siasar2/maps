<?xml version="1.0" encoding="UTF-8"?>
  <StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
      <Name>data:communities</Name>
      <UserStyle>
        <Title>Heatmap</Title>
        <Abstract>A heatmap surface showing population density</Abstract>
        <FeatureTypeStyle>
          <Transformation>
            <ogc:Function name="gs:Heatmap">
              <ogc:Function name="parameter">
                <ogc:Literal>data</ogc:Literal>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>radiusPixels</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>radius</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
              <!-- <ogc:Function name="parameter">
                <ogc:Literal>weightAttr</ogc:Literal>
                <ogc:Literal>population</ogc:Literal>
              </ogc:Function> -->
              <ogc:Function name="parameter">
                <ogc:Literal>pixelsPerCell</ogc:Literal>
                <ogc:Literal>1</ogc:Literal>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputBBOX</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_bbox</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputWidth</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_width</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputHeight</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_height</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
            </ogc:Function>
          </Transformation>
          <Rule>
            <RasterSymbolizer>
              <!-- specify geometry attribute to pass validation -->
              <Geometry>
                <ogc:PropertyName>geom</ogc:PropertyName>
              </Geometry>
              <Opacity>1</Opacity>
              <ColorMap type="ramp">
                <ColorMapEntry color="#FFFFFF" quantity="0" label="0" opacity="0" />
                <ColorMapEntry color="#000080" quantity="0.1" label="0 - 25" opacity="0.3" />
                <ColorMapEntry color="#0000FF" quantity=".25" label="25 - 50" opacity="0.3" />
                <ColorMapEntry color="#008000" quantity=".5" label="50 - 75" opacity="0.7" />
                <ColorMapEntry color="#FFFF00" quantity=".75" label="50 - 75" opacity="0.8" />
                <ColorMapEntry color="#FF0000" quantity="1.0" label="75 - 100" opacity="1" />
              </ColorMap>
            </RasterSymbolizer>
          </Rule>
        </FeatureTypeStyle>
      </UserStyle>
    </NamedLayer>
  </StyledLayerDescriptor>
