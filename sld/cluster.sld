<?xml version="1.0" encoding="UTF-8"?>
  <StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
      <Name>Cluster</Name>
      <UserStyle>
        <Title>Cluster</Title>
        <Abstract>A cluster example</Abstract>
        <FeatureTypeStyle>
          <Transformation>
            <ogc:Function name="vec:PointStacker">
              <ogc:Function name="parameter">
                <ogc:Literal>data</ogc:Literal>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>cellSize</ogc:Literal>
                <ogc:Literal>50</ogc:Literal>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputBBOX</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_bbox</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputWidth</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_width</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
              <ogc:Function name="parameter">
                <ogc:Literal>outputHeight</ogc:Literal>
                <ogc:Function name="env">
                  <ogc:Literal>wms_height</ogc:Literal>
                </ogc:Function>
              </ogc:Function>
            </ogc:Function>
          </Transformation>
          <Rule>
            <Name>rule1</Name>
            <Title>Volcano</Title>
            <ogc:Filter>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>count</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:Filter>
            <PointSymbolizer>
              <Graphic>
                <Mark>
                  <WellKnownName>circle</WellKnownName>
                  <Fill>
                    <CssParameter name="fill">#096093</CssParameter>
                  </Fill>
                </Mark>
                <Size>12</Size>
              </Graphic>
            </PointSymbolizer>
          </Rule>
          <Rule>
            <Name>rule29</Name>
            <Title>2-9 Volcanoes</Title>
            <ogc:Filter>
              <ogc:PropertyIsBetween>
                <ogc:PropertyName>count</ogc:PropertyName>
                <ogc:LowerBoundary>
                  <ogc:Literal>2</ogc:Literal>
                </ogc:LowerBoundary>
                <ogc:UpperBoundary>
                  <ogc:Literal>100</ogc:Literal>
                </ogc:UpperBoundary>
              </ogc:PropertyIsBetween>
            </ogc:Filter>
            <PointSymbolizer>
              <Graphic>
                <Mark>
                  <WellKnownName>circle</WellKnownName>
                  <Fill>
                    <CssParameter name="fill">#096093</CssParameter>
                  </Fill>
                </Mark>
                <Size>24</Size>
              </Graphic>
            </PointSymbolizer>
            <TextSymbolizer>
              <Label>
                <ogc:PropertyName>count</ogc:PropertyName>
              </Label>
              <Font>
              <CssParameter name="font-family">Nunito-Regular</CssParameter>
              <CssParameter name="font-size">14</CssParameter>
              <CssParameter name="font-weight">bold</CssParameter>
            </Font>
              <LabelPlacement>
                <PointPlacement>
                  <AnchorPoint>
                    <AnchorPointX>0.5</AnchorPointX>
                    <AnchorPointY>0.8</AnchorPointY>
                  </AnchorPoint>
                </PointPlacement>
              </LabelPlacement>
              <Halo>
                <Radius>2</Radius>
                <Fill>
                  <CssParameter name="fill">#096093</CssParameter>
                  <CssParameter name="fill-opacity">0.9</CssParameter>
                </Fill>
              </Halo>
              <Fill>
                <CssParameter name="fill">#FFFFFF</CssParameter>
                <CssParameter name="fill-opacity">1.0</CssParameter>
              </Fill>
            </TextSymbolizer>
          </Rule>
          <Rule>
            <Name>rule10</Name>
            <Title>10 Volcanoes</Title>
            <ogc:Filter>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>count</ogc:PropertyName>
                <ogc:Literal>100</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
            </ogc:Filter>
            <PointSymbolizer>
              <Graphic>
                <Mark>
                  <WellKnownName>circle</WellKnownName>
                  <Fill>
                    <CssParameter name="fill">#096093</CssParameter>
                  </Fill>
                </Mark>
                <Size>40</Size>
              </Graphic>
            </PointSymbolizer>
            <TextSymbolizer>
              <Label>
                <ogc:PropertyName>count</ogc:PropertyName>
              </Label>
              <Font>
              <CssParameter name="font-family">Nunito-Regular</CssParameter>
              <CssParameter name="font-size">16</CssParameter>
              <CssParameter name="font-weight">bold</CssParameter>
            </Font>
              <LabelPlacement>
                <PointPlacement>
                  <AnchorPoint>
                    <AnchorPointX>0.5</AnchorPointX>
                    <AnchorPointY>0.8</AnchorPointY>
                  </AnchorPoint>
                </PointPlacement>
              </LabelPlacement>
              <Halo>
                <Radius>2</Radius>
                <Fill>
                  <CssParameter name="fill">#096093</CssParameter>
                  <CssParameter name="fill-opacity">0.9</CssParameter>
                </Fill>
              </Halo>
              <Fill>
                <CssParameter name="fill">#FFFFFF</CssParameter>
                <CssParameter name="fill-opacity">1.0</CssParameter>
              </Fill>
            </TextSymbolizer>
          </Rule>
        </FeatureTypeStyle>
      </UserStyle>
    </NamedLayer>
  </StyledLayerDescriptor>
