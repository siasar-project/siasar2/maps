import * as GeoDashboard from 'geodashboard';
import SiasarDashboard from '../lib/siasar-dashboard';

const dashboard = new SiasarDashboard({
  dashboard: {
    map: {
      center: [103.378253, 20.0171109],
      zoom: 7,
    },
    filters: [new GeoDashboard.Filter({
      property: 'country',
      value: 'LA',
    })],
  },
});

dashboard.init().then(() => {
  dashboard.addBaseLayers();
  dashboard.addOverlayLayers();
  dashboard.addWidgets();
  dashboard.render();
});
