import * as GeoDashboard from 'geodashboard';
import SiasarDashboard from '../lib/siasar-dashboard';

const dashboard = new SiasarDashboard({
  dashboard: {
    map: {
      center: [74.520, 41.298],
      zoom: 7,
    },
  },
});

dashboard.addFilter(new GeoDashboard.Filter({
  property: 'country',
  value: 'KG',
}));

dashboard.init().then(() => {
  dashboard.addBaseLayers();
  dashboard.addOverlayLayers();
  dashboard.addWidgets();
  dashboard.render();
});
