import * as GeoDashboard from 'geodashboard';
import SiasarDashboard from '../lib/siasar-dashboard';

const dashboard = new SiasarDashboard({
  dashboard: {
    map: {
      center: [33.7, -6.04],
      zoom: 6,
    },
  },
});

dashboard.addFilter(new GeoDashboard.Filter({
  property: 'country',
  value: 'TZ',
}));

dashboard.init().then(() => {
  dashboard.addBaseLayers();
  dashboard.addOverlayLayers();
  dashboard.addWidgets();
  dashboard.render();
});
